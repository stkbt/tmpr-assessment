# Tmpr PHP Assessment

PHP Assessment for temper.works

Built on Lumen Framework & Vue.js

## Setup Instructions

* clone repository
* execute `composer install`
* serve `php -S localhost:8000 -t public`

## Tests

Cover API HTTP-response

* phpunit

## Screenshot

![alt text](/screenshot.jpg)