<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TestHttp extends TestCase
{

    /**
     * Test application
     *
     * @return void
     */
    public function testApplication()
    {
        $response = $this->call('GET', '/');

        $this->assertEquals(200, $response->status());
    }


    /**
     * Test onboarding retention response
     *
     * @return void
     */
    public function testOnboardingResponse()
    {
        $response = $this->call('GET', '/api/onboarding');

        $this->assertEquals(200, $response->status());

        $this->seeJsonStructure([
            'series',
            'x'
        ]);
    }


}
