let mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/assets/');
mix.sass('resources/scss/app.scss', 'public/assets/');

if (mix.inProduction()) {
	
	mix.version();
} 
else {
	
	mix.sourceMaps();
}