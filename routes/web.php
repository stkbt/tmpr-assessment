<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


// Routes for API

$router->group(['prefix' => 'api'], function ($router) {

    $router->get('/onboarding', 'ApiController@onboarding');

});



// Route any other request to Vue (VueRouter)

$router->get('/{route:.*}/', function ()  {
    return view('app');
});