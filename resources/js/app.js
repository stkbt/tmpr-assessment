import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store';

Vue.use(VueRouter);

import OnboardingChartLines from './components/OnboardingChartLines';
import OnboardingChartBars from './components/OnboardingChartBars';

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes: [
        {path: '/lines', component: OnboardingChartLines},
        {path: '/bars', component: OnboardingChartBars}
    ]
});

new Vue({
    router,
    store
}).$mount('#app');