import { getDataOnboarding } from '../../api/data'

// initial state
const state = {
  data: []
}

// getters
const getters = {

  onboardingData: state => state.data
};

// actions
const actions = {
  getData ({ commit }) {

    getDataOnboarding().then((data)=>{

      commit('setDataOnboarding', data);
    });
    
  }
}

// mutations
const mutations = {

  setDataOnboarding(state, data) {

    state.data = data;
  }
}

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
}