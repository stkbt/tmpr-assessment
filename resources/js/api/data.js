import axios from 'axios';

export function getDataOnboarding(data) {

	return new Promise((resolve, reject)=>{

		axios.get('/api/onboarding')
			.then((response)=>{

				resolve(response.data);
			})
			.catch((error)=>{

				reject(error);
			});
	});
}