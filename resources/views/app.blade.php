<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>tmpr –– PHP Assessment</title>
    <link rel="stylesheet" href="{{ mix('assets/app.css') }}" />
</head>
<body>
    <h1>tmpr –– PHP Assessment</h1>

    <div id="app">
    	
        <p class="nav">
            <router-link to="/lines">Onboarding Retention (lines)</router-link> 
            <router-link to="/bars">Onboarding Retention (bars)</router-link>
        </p>

        <router-view></router-view>

    </div>

    <script src="{{ mix('assets/app.js') }}"></script>
</body>
</html>