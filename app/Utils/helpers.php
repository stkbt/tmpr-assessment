<?php

if (!function_exists('app_path')) {
    /**
     * Get the path to the application folder.
     *
     * @param string $path
     *
     * @return string
     */
    function app_path($path = '')
    {
        return app('path') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if (!function_exists('public_path')) {
    /**
     * Get the path to the public folder.
     *
     * @param string $path
     *
     * @return string
     */
    function public_path($path = '')
    {
        return app()->basePath() . '/public' . ($path ? DIRECTORY_SEPARATOR . ltrim($path, DIRECTORY_SEPARATOR) : $path);;
    }
}

if (!function_exists('asset')) {
    /**
     * Generate an asset path for the application.
     *
     * @param string $path
     * @param bool $secure
     *
     * @return string
     */
    function asset($path, $secure = null)
    {
        return (new UrlGenerator(app()))->to($path, null, $secure);
    }
}

if (!function_exists('mix')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param string $path
     * @param string $manifestDirectory
     *
     * @return HtmlString|string
     *
     * @throws Exception
     */
    function mix($path, $manifestDirectory = '')
    {
        static $manifests = [];
        if (!\Str::startsWith($path, '/')) {
            $path = "/{$path}";
        }
        if ($manifestDirectory && !Str::startsWith($manifestDirectory, '/')) {
            $manifestDirectory = "/{$manifestDirectory}";
        }
        if (file_exists(public_path($manifestDirectory . '/hot'))) {
            $port = config('mix.port', 8080);
            $host = config('mix.host', 'localhost');
            return new HtmlString("//{$host}:{$port}{$path}");
        }
        $manifestPath = public_path($manifestDirectory . '/mix-manifest.json');
        if (!isset($manifests[$manifestPath])) {
            if (!file_exists($manifestPath)) {
                throw new Exception('The Mix manifest does not exist.');
            }
            $manifests[$manifestPath] = json_decode(file_get_contents($manifestPath), true);
        }
        $manifest = $manifests[$manifestPath];
        if (!isset($manifest[$path])) {
            report(new Exception("Unable to locate Mix file: {$path}."));
            if (!app('config')->get('app.debug')) {
                return $path;
            }
        }
        return new HtmlString($manifestDirectory . $manifest[$path]);
    }
}