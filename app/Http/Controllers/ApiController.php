<?php

namespace App\Http\Controllers;

use App\DataProviders\CsvDataProvider;

class ApiController extends Controller
{
    
    
    /**
     * Request data for Onboarding Retention Chart
     *
     * @return json response
     */
    public function onboarding() {

        $data_provider = new CsvDataProvider(storage_path('temp/export.csv'));

        $data = $data_provider->getRetentionByWeeks();


        return response()->json([
            'series' => $data,
            'x' => $data_provider->getOnboardingStepsTitles()
        ]);
    }


}
