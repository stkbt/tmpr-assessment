<?php

namespace App\Http\Middleware;

use Closure;

class ApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $is_allow = true; // Here we can provide some Authorization solutions

        if(!$is_allow) {

            return response('Access denied', 403);
        }

        return $next($request);
    }
}
