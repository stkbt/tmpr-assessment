<?php

namespace App\DataProviders\Contracts;

interface DataContract
{

    /**
     * Get & return retention data by weeks
     *
     * @return array
     */
    public function getDataByWeeks();
}
