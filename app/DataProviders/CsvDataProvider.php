<?php

namespace App\DataProviders;

class CsvDataProvider extends DataProvider {


	private $filepath;
	private $delimiter = ';';
	

	/**
     * Create a new data provider
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(string $filepath, string $delimiter = ';')
    {
        
        $this->filepath = $filepath;
        $this->delimiter = $delimiter;
    }



    /**
     * Get data from source
     *
     * @return array
     */
	public function getDataByWeeks() {

		return $this->parseCsv();
	}



	/** 
     * Read data from CSV file and convert to array by weeks.
     *
     * @return array
    */

    private function parseCsv(int $length = 1000):array
    {
        ini_set('auto_detect_line_endings', true);

        if(!file_exists($this->filepath) || !is_readable($this->filepath)) {
            return [];
        }

        $header = null;
        $data = [];

        if(($handle = fopen($this->filepath, 'r')) !== false)
        {

            $year_week_previous = null;

            while (($row = fgetcsv($handle, $length, $this->delimiter)) !== false)
            {
                if(!$header) {
                    $header = $row;
                }
                else {
                    $item = array_combine($header, $row);

                    if(!isset($item['created_at'])) {
                        continue;
                    }

                    $year_week_item = date("o-W", strtotime($item['created_at']));

                    if(!$year_week_previous) {
                        $year_week_previous = $year_week_item;
                    }

                    if(!isset($data[$year_week_item])) {
                        $data[$year_week_item] = [];
                    }

                    $data[$year_week_item][] = intval($item['onboarding_perentage']);
                }
            }
            fclose($handle);
        }

        return $data;
    }
}