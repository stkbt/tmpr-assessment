<?php

namespace App\DataProviders;

use App\DataProviders\Contracts\DataContract;


abstract class DataProvider implements DataContract {

	// onboarding steps (for test in const, by can be provided from database)

    public const ONBOARDING_STEPS = [
        [
            'title' => 'Create account',
            'percentage' => 0
        ],
        [
            'title' => 'Activate account',
            'percentage' => 20
        ],
        [
            'title' => 'Provide profile information',
            'percentage' => 40
        ],
        [
            'title' => 'What jobs are you interested in?',
            'percentage' => 50
        ],
        [
            'title' => 'Do you have relevant experience in these jobs?',
            'percentage' => 70
        ],
        [
            'title' => 'Are you a freelancer?',
            'percentage' => 90
        ],
        [
            'title' => 'Waiting for approval',
            'percentage' => 99
        ],
        [
            'title' => 'Approval',
            'percentage' => 100
        ],
    ];


    /**
     * Get data from source groupped by weeks
     *
     * @return array
     */
	public function getDataByWeeks() {

		return [];
	}



	/**
     * Calculate retention data
     *
     * @return array
     */
	public function getRetentionByWeeks() {
		
		$data = $this->getDataByWeeks();

		array_walk($data, function(&$values, &$week) {

            $percentages_by_step = [];

            foreach(self::ONBOARDING_STEPS as $step) {
                
                $values_in_step = array_filter($values, function($value) use ($step) {
                    return $value >= $step['percentage'];
                });

                $percentages_by_step[] = round(100*count($values_in_step)/count($values));
            }

            $values = [
                'name' => $this->getWeekHumanReadable($week),
                'data' => $percentages_by_step
            ];
        });

        return array_values($data);
	}

	/**
     * Get onboarding steps titles
     *
     * @return array
     */
	public function getOnboardingStepsTitles() {
		
		$steps = self::ONBOARDING_STEPS;

        array_walk($steps, function(&$value, $index) {
            $value = ($index+1) . '. ' . $value['title'];
        });


        return $steps;
	}


	/** 
     * Get start & end dates of week by week number
     *
     * @return array
    */

    private function getWeekHumanReadable($week_str) {

        $week_arr = explode('-', $week_str);

        $datetime = new \DateTime();

        $result[] = $datetime->setISODate($week_arr[0], $week_arr[1])->format('d/m/Y');
        $result[] = $datetime->modify('+6 days')->format('d/m/Y');

        return implode(' - ', $result);
    }


    /** 
     * Calculate median value for the array
     *
     * @return integer
    */

    private function calculateMedianValue($arr) {

        if($arr){

            $count = count($arr);

            sort($arr);

            $mid = floor(($count-1)/2);

            return ($arr[$mid] + $arr[$mid+1-$count%2])/2;
        }
        return 0;
    }

}